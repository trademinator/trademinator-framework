<?php
if (!function_exists('bcconv')) {

	function bcconv($fNumber){
		$sAppend = '';
		$iDecimals = ini_get('precision') - floor(log10(abs($fNumber)));
		if (0 > $iDecimals){
			$fNumber *= pow(10, $iDecimals);
			$sAppend = str_repeat('0', -$iDecimals);
			$iDecimals = 0;
		}
		return number_format($fNumber, intval($iDecimals), '.', '').$sAppend;
	}
}
