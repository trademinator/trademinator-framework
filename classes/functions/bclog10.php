<?php
if (!function_exists('bclog10')) {
	function bclog10($n){
		$pos=strpos($n,'.');
		if($pos===false){
		    $dec_frac='.'.substr($n,0,15);$pos=strlen($n);
		}
		else{
			$dec_frac='.'.substr(substr($n,0,$pos).substr($n,$pos+1),0,15);
		}
		return log10((float)$dec_frac)+(float)$pos;
	}
}
