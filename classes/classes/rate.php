<?php
$server_path = dirname(__FILE__);
set_include_path(get_include_path() . PATH_SEPARATOR . $server_path . '/../' . PATH_SEPARATOR . $server_path . '/');

require_once ('config.php');
require_once ('resources/functions/stats_standard_deviation.php');
require_once ('resources/functions/bcmax_min.php');
require_once ('resources/functions/bcabs.php');
require_once ('resources/functions/bclog10.php');

/**
  *  Rate class. A rate class contains all information about the currency valuation against another
*/

class Rate{
	private $exchange;
	private $from;
	private $to;
	private $last;
	private $lowest_ask;
	private $highest_bid;
	private $percent_change;
	private $base_volume;
	private $quote_volume;
	private $is_frozen;
	private $high_24hr;
	private $low_24hr;
	private $historial;
	private $resistance;
	private $resistance0;
	private $support;
	private $support0;
	private $pair;
	private $period;
	private $my_trades;
	private $base_volume_market_share;
	private $breaking_point;				// Paying / (got - fee) ??
	private $book;
	private $spread;
	private $percent_wise;
	private $start;
	private $end;

	function __construct(&$exchange, $from, $to, $last, $lowest_ask, $highest_bid, $percent_change, $base_volume, $quote_volume, $is_frozen, $high_24hr, $low_24hr){
		if ((DEBUG & DEBUG_RATE) && (DEBUG & DEBUG_TRACE_FUNCTIONS))
			syslog(LOG_INFO|LOG_LOCAL1, "function rate::__construct(\$exchange, $from, $to, $last, $lowest_ask, $highest_bid, $percent_change, $base_volume, $quote_volume, $is_frozen, $high_24hr, $low_24hr)");
		$this->exchange = &$exchange;
		$this->from = $from;
		$this->to = $to;
		$this->last = $last;
		$this->lowest_ask = $lowest_ask;
		$this->highest_bid = $highest_bid;
		$this->percent_change = $percent_change;
		$this->base_volume = $base_volume;
		$this->quote_volume = $quote_volume;
		$this->is_frozen = $is_frozen;
		$this->high_24hr = $high_24hr;
		$this->low_24hr = $low_24hr;
		$this->historial = array();
		$this->resistance = (double) 0.0;
		$this->support = (double) 0.0;
		$this->pair = $this->exchange->build_pair_string($from,$to);
		$this->period = 900;
		$this->my_trades = array();
		$this->refresh();
		$this->base_volume_market_share = 0;
		$this->book = array();
		$this->spread = bcsub($lowest_ask, $highest_bid, EXCHANGE_ROUND_DECIMALS * 2);
		$this->percent_wise = bcdiv($this->spread, $lowest_ask, EXCHANGE_ROUND_DECIMALS * 2);
	}

	public function get_historic_data($start, $end){
		if ((DEBUG & DEBUG_RATE) && (DEBUG & DEBUG_TRACE_FUNCTIONS))
			syslog(LOG_INFO|LOG_LOCAL1, "public function &rate::get_historic_data($start, $end)");
		$answer = $this->exchange->get_historic_data($this->pair, $start, $end, $this->period);
		if ((DEBUG & DEBUG_RATE) && (DEBUG & DEBUG_TRACE_FUNCTIONS_OUTPUT))
			syslog(LOG_INFO|LOG_LOCAL1, "public function &rate::get_historic_data($start, $end) = ".print_r($answer, true));
		return $answer;
	}

	public function rerefresh($elements = RATE_REFRESH_ALL){
		if (is_null($this->period) || is_null($this->end) || is_null($this->start))
			throw new Exception('You must run rate::refresh() first');

		return $this->refresh($this->period, $this->end, $this->start, $elements);
	}

	/**
	@param number $period
	number in seconds, 300, 900, 1800, 7200 or 14400
	@param number $end
	timestamp of the ending date, can not be greater than now
	@param number $start
	timestamp of the starting date, can not be greater or equal than end
	@retval object Wallet
	The wallet object corresponding to that currency
	*/
	public function refresh($period = 900, $end = null, $start = null, $elements = RATE_REFRESH_ALL){
		if ((DEBUG & DEBUG_RATE) && (DEBUG & DEBUG_TRACE_FUNCTIONS))
			syslog(LOG_INFO|LOG_LOCAL1, "public function rate::refresh($period, $end, $start, $elements)");
		date_default_timezone_set('GMT');
		$date = new DateTime();
		if (is_null($end)){
			$end = $date->getTimestamp();
		}
		else{
			if ($end > $date->getTimestamp()){
				$end = $date->getTimestamp();
			}
		}

		if (is_null($start)){
			$start = $end - 1296000;
		}
		else{
		      if ($start >= $end){
				$start = $end - 1296000;
		      }
		}
		$this->end = $end;
		$this->start = $start;

		switch($period){
			case 300:
			case 900:
			case 1800:
			case 7200:
			case 14400:
				$this->period = $period;
				break;
			default:
				$period = 900;
				$this->period = 900;
		}

		/* TODO: Be sure we get at least 30 samples */
		if ($elements & RATE_REFRESH_TICKER){
			$this->historial = $this->get_historic_data($start, $end);
			$max = 0; $min = 999999999; $nReg = count($this->historial);
			for($i = 0 ; $i < $nReg; $i++){
				$h = $this->historial[$i];
				$max = bcmax($max, $h['high']);
				$min = bcmin($min, $h['low']);
			}

			$this->resistance0 = $max;
			$this->support0 = $min;
			$five = bcmul(bcsub($max, $min, EXCHANGE_ROUND_DECIMALS * 2), 0.05, EXCHANGE_ROUND_DECIMALS * 2);
			$this->resistance = bcsub($max, $five, EXCHANGE_ROUND_DECIMALS * 2);
			$this->support = bcadd($min, $five, EXCHANGE_ROUND_DECIMALS * 2);
		}

		if (!$this->exchange->is_public()){
			if ($elements & RATE_REFRESH_MY_TRADES){
				$this->get_my_trade_history();
			}
		}

		if ($elements & RATE_REFRESH_ORDERS){
			$this->book = $this->get_orders();
		}
	}

	/**
	@retval array
	All the historical information
	*/
	public function get_my_trade_history($end = null, $start = null){
		if ((DEBUG & DEBUG_RATE) && (DEBUG & DEBUG_TRACE_FUNCTIONS))
			syslog(LOG_INFO|LOG_LOCAL1, "public function rate::get_my_trade_history($end,$start)");

		if ($this->exchange->is_public()){
			throw new Exception('This exchange is in public mode.');
			return array();
		}
		else{
			$date = new DateTime();
			if (is_null($end)){
				$end = $date->getTimestamp();
			}
			else{
				if ($end > $date->getTimestamp()){
					$end = $date->getTimestamp();
				}
			}

			if (is_null($start)){
				$start = $end - 1296000; // 15 days in seconds
			}
			else{
			      if ($start >= $end){
					$start = $end - 1296000;
			      }
			}

			$this->my_trades = $this->exchange->get_my_trade_history($this->pair, $end, $start);
			if ((DEBUG & DEBUG_RATE) && (DEBUG & DEBUG_TRACE_FUNCTIONS_OUTPUT))
				syslog(LOG_INFO|LOG_LOCAL1, "public function rate::get_my_trade_history($end,$start) = ".print_r($this->myTrades,true));
		}

		return $this->my_trades;
	}

	/**
	@retval array
	All the historical information
	*/
	public function &historial(){
		if (count($this->historial) == 0){
			$this->refresh();
		}
		return $this->historial;
	}

	/**
	@retval string
	The ISO code of the destination currency
	*/
	public function &to(){
		return $this->to;
	}

	/**
	@retval string
	The ISO code of the origin currency
	*/
	public function &from(){
		return $this->from;
	}

	/**
	@retval string
	The pair XXX_YYY
	*/
	public function &pair(){
		return $this->pair;
	}

	/**
	@retval number
	The resistance point calculated from the current historial
	*/
	public function &resistance(){
		return $this->resistance;
	}

	/**
	@retval number
	The support point calculated from the current historial
	*/
	public function &support(){
		return $this->support;
	}

	/**
	@retval boolean
	Returns if this currency is considered liquid
	*/
	public function is_liquid(){
		return ($this->lowest_ask <= $this->highest_bid)?true:false;		// This is only one of many conditions to determine if it is or not
	}

	/**
	@retval number
	Last rate price
	*/
	public function last(){
		return $this->last;
	}

	/**
	@retval number
	Last rate price
	*/
	public function percent_change(){
		return $this->percent_change;
	}

	/**
	@retval number
	Highest Bid
	*/
	public function highest_bid(){
		return $this->highest_bid;
	}

	/**
	@retval number
	Lowest Ask
	*/
	public function lowest_ask(){
		return $this->lowest_ask;
	}

	/**
	@retval array
	Past trades
	*/
	public function my_trades(){
		if (count($this->my_trades) == 0){
			$this->get_my_trade_history();
		}
		return $this->my_trades;
	}

	/**
	@param number $maxShare
	100% value
	@retval object
	This
	*/
	public function &set_market_share($max){
		if (bccomp($max, 0, EXCHANGE_ROUND_DECIMALS * 2) > 0){
			$this->base_volume_market_share = bcdiv($this->base_volume, $max, 4); // Between 0 and 1 
		}
		return $this;
	}

	/**
	@retval number
	Percentage of Volume Market Share
	*/
	public function &base_volume_market_share(){
		return $this->base_volume_market_share;
	}

	/**
	@retval number
	Percentage of Volume Market Share
	*/
	public function &get_market_share(){
		return $this->base_volume_market_share;
	}

	/**
	@param float
	@retval boolean
	Returns if price is equal or bellow the percentage band
	*/
	public function &is_bellow_percentage_band($percentage){
		$one = ($this->resistance0 - $this->support0) / 100;
		$c = $this->last();
		$r = ($c <= ($this->support0 + $one * $percentage))?true:false;
		return $r;
	}

	/**
	@retval array
	array of historial
	*/
	public function get_historial(){
		return $this->historial;
	}

	/**
	@retval integer
	*/
	public function period(){
		return $this->period;
	}

	/**
	@retval integer
	*/
	public function base_volume(){
		return $this->base_volume;
	}

	/**
	@retval array
	*/
	public function book(){
		$this->book = $this->get_orders();
		return $this->book;
	}

	/**
	@retval float
	*/
	public function spread(){
		$this->book = $this->get_orders();
		return $this->spread;
	}

	/**
	@retval float
	*/
	public function percent_wise(){
		$this->book = $this->get_orders();
		return $this->percent_wise;
	}

	public function delete_key(...$key){
		reset($this->historial);
		foreach ($this->historial as &$h){
			foreach ($key as $k)
				unset($h[$k]);
		}
	}

	private function rename_key($oldkey, $newkey){
		reset($this->historial);
		foreach ($this->historial as &$h){
			$h[$newkey] = $h[$oldkey];
			unset($h[$oldkey]);
		}
	}

	private function clone_key($oldkey, $newkey){
		reset($this->historial);
		foreach ($this->historial as &$h){
			$h[$newkey] = $h[$oldkey];
		}
	}

	public function normalize($key = 'close', $index = 'close'){
		if ((DEBUG & DEBUG_RATE) && (DEBUG & DEBUG_TRACE_FUNCTIONS))
			syslog(LOG_INFO|LOG_LOCAL1, "public function rate::normalize($key, $index)");

		$nkey = 'normalized('.$key.','.$index.')';
		$t = end($this->historial);
		if (!array_key_exists($nkey, $t)){
			reset($this->historial);
			foreach ($this->historial as &$h){	// Last element is the most recent
				if (bccomp($h[$index], 0, EXCHANGE_ROUND_DECIMALS * 2) > 0){
					$h[$nkey] = bcdiv($h[$key], $h[$index], EXCHANGE_ROUND_DECIMALS * 2);
				}
				else{
					$h[$nkey] = 0;
				}
			}
		}

		if ((DEBUG & DEBUG_RATE) && (DEBUG & DEBUG_TRACE_FUNCTIONS_OUTPUT))
			syslog(LOG_INFO|LOG_LOCAL1, "public function rate::normalize($key, $index)");

		return $nkey;
	}

	public function ema($period = 2, $index = 'close'){
		if ((DEBUG & DEBUG_RATE) && (DEBUG & DEBUG_TRACE_FUNCTIONS))
			syslog(LOG_INFO|LOG_LOCAL1, "public function rate::ema($period, $index)");

		$t = end($this->historial);
		$key = 'ema('.$period.','.$index.')';
		if (!array_key_exists($key, $t)){
			if ($period == 1){
				$this->clone_key($index, $key);
			}
			else{
				$i = 1;
				//K = 2 ÷(N + 1)
				$a = bcdiv(2, bcadd(1, $period, EXCHANGE_ROUND_DECIMALS * 2), EXCHANGE_ROUND_DECIMALS * 2);
				reset($this->historial);
				foreach ($this->historial as &$h){	// Last element is the most recent
					if ($i == 1){
						$h[$key] = number_format($h[$index], EXCHANGE_ROUND_DECIMALS, '.', '');
					}
					else{
						//EMA [today] = (Price [today] x K) + (EMA [yesterday] x (1 – K))
						$h[$key] = bcadd(bcmul($a,  $h[$index], EXCHANGE_ROUND_DECIMALS * 2), bcmul(bcsub(1, $a, EXCHANGE_ROUND_DECIMALS * 2), $p[$key], EXCHANGE_ROUND_DECIMALS * 2), EXCHANGE_ROUND_DECIMALS * 2);
					}
					$p = $h; $i++;
				}
			}
		}
		if ((DEBUG & DEBUG_RATE) && (DEBUG & DEBUG_TRACE_FUNCTIONS_OUTPUT))
			syslog(LOG_INFO|LOG_LOCAL1, "public function rate::ema($period, $base_key, $index)");

		return $key;
	}

	/**
	@retval string
	Returns the Typical Price
	*/
	public function tp(){
		$key = 'tp()';
		$t = end($this->historial);

		if (!array_key_exists($key, $t)){
			reset($this->historial);
			foreach ($this->historial as &$h){	// Last element is the most rescent
				$t = bcadd($h['high'], $h['low'], EXCHANGE_ROUND_DECIMALS * 2);
				$t = bcadd($t, $h['close'], EXCHANGE_ROUND_DECIMALS * 2);
				$h[$key] = bcdiv($t, 3, EXCHANGE_ROUND_DECIMALS * 2);
			}
		}
		return $key;
	}

	/**
	@retval string
	Returns the Choppiness Index
	*/
	public function chop($period = 20){
		if ((DEBUG & DEBUG_RATE) && (DEBUG & DEBUG_TRACE_FUNCTIONS))
			syslog(LOG_INFO|LOG_LOCAL1, "public function rate::chop($period)");

		$key = 'chop('.$period.')';
		$t = end($this->historial);
		if (!array_key_exists($key, $t)){
			$buffer = array();
			$key_tr = $this->tr();
			$bclog10_period = bclog10($period);
			list($key_min_max_high_min, $key_min_max_high_max, $key_min_max_high_steps_min, $key_min_max_high_steps_max, $key_abs_min_max_high_min, $key_abs_min_max_high_max, $key_abs_min_max_high_steps_min, $key_abs_min_max_high_steps_max) = $this->min_max($period, 'high', EXCHANGE_ROUND_DECIMALS * 2);
			list($key_min_max_low_min, $key_min_max_low_max, $key_min_max_low_steps_min, $key_min_max_low_steps_max, $key_abs_min_max_low_min, $key_abs_min_max_low_max, $key_abs_min_max_low_steps_min, $key_abs_min_max_low_steps_max) = $this->min_max($period, 'low', EXCHANGE_ROUND_DECIMALS * 2);

			reset($this->historial);
			foreach ($this->historial as &$h){
				array_push($buffer, $h[$key_tr]);
				if (count($buffer) > $period){
					array_shift($buffer);
				}
				$sum = 0;
				foreach ($buffer as $b){
					$sum = bcadd($sum, $b, EXCHANGE_ROUND_DECIMALS * 2);
				}

				$h[$key] = bcmul(100, bcdiv(bclog10(bcdiv($sum, bcsub($h[$key_min_max_high_max], $h[$key_min_max_low_min], EXCHANGE_ROUND_DECIMALS * 2), EXCHANGE_ROUND_DECIMALS * 2)), $bclog10_period, EXCHANGE_ROUND_DECIMALS * 2), 2);
			}
		}

		return $key;
	}

	/**
	@retval string
	Returns the Commodity Channel Index
	*/
	public function cci($period = 20){
		if ((DEBUG & DEBUG_RATE) && (DEBUG & DEBUG_TRACE_FUNCTIONS))
			syslog(LOG_INFO|LOG_LOCAL1, "public function rate::cci($period)");

		$key = 'cci('.$period.')';
		$t = end($this->historial);

		if (!array_key_exists($key, $t)){
			$buffer = array();
			$tp = $this->tp();
			$tp_sma = $this->sma($period, $tp);
			reset($this->historial);
			foreach ($this->historial as &$h){	// Last element is the most rescent
				array_push($buffer, $h[$tp_sma]);
				if (count($buffer) > $period){
					array_shift($buffer);
				}
				if (count($buffer) > 1){
					$std = stats_standard_deviation($buffer, true);
					$h[$key] = bcdiv(bcsub($h[$tp], $h[$tp_sma], EXCHANGE_ROUND_DECIMALS * 2), bcmul(bcconv($std), bcconv(0.015), EXCHANGE_ROUND_DECIMALS * 2), EXCHANGE_ROUND_DECIMALS * 2);
				}
			}
		}
		return $key;
	}

	/**
	@retval array
	Returns the TR
	*/
	public function tr(){
		if ((DEBUG & DEBUG_RATE) && (DEBUG & DEBUG_TRACE_FUNCTIONS))
			syslog(LOG_INFO|LOG_LOCAL1, "public function rate::tr()");

		$i = 1; $key = 'tr()';
		$t = end($this->historial);
		if (!array_key_exists($key, $t)){
			reset($this->historial);
			foreach ($this->historial as &$h){	// Last element is the most rescent
				if ($i == 1){
					$previous_close = $h['low'];
				}
				else{
					$previous_close = $p['close'];
				}
				$tr1 = bcsub($h['high'], $h['low'], 8);
				$tr2 = bcabs(bcsub($h['high'], $previous_close, 8));
				$tr3 = bcabs(bcsub($previous_close, $h['low'], 8));
				$p = $h; $i++;
				$h[$key] = bcmax($tr1, $tr2, $tr3);
			}
		}

		if ((DEBUG & DEBUG_RATE) && (DEBUG & DEBUG_TRACE_FUNCTIONS_OUTPUT))
			syslog(LOG_INFO|LOG_LOCAL1, "public function rate::tr() = ".print_r($this->historial, true));

		return $key;
	}

	/**
	@retval array
	Returns the +DM and -DM
	*/
	public function dm(){
		if ((DEBUG & DEBUG_RATE) && (DEBUG & DEBUG_TRACE_FUNCTIONS))
			syslog(LOG_INFO|LOG_LOCAL1, "public function rate::dm()");

		$mkey = '-dm()'; $pkey = '+dm()';
		$t = end($this->historial);

		if (!array_key_exists($mkey, $t) or !array_key_exists($pkey, $t)){
			$i = 1;
			reset($this->historial);
			foreach ($this->historial as &$h){	// Last element is the most rescent
				if ($i == 1){
					$h[$pkey] = $h['close'];
					$h[$mkey] = $h['close'];
				}
				else{
					$upmove = bcsub($h['high'], $p['high'], EXCHANGE_ROUND_DECIMALS * 2);
					$downmove = bcsub($p['low'], $h['low'], EXCHANGE_ROUND_DECIMALS * 2);
					$h[$pkey] = 0; $h[$mkey] = 0;
					if ((bccomp($upmove, $downmove, EXCHANGE_ROUND_DECIMALS * 2) > 0) and (bccomp($upmove, 0, EXCHANGE_ROUND_DECIMALS * 2) > 0)){
						$h[$pkey] = $upmove;
					}

					if ((bccomp($downmove, $upmove, EXCHANGE_ROUND_DECIMALS * 2) > 0) and (bccomp($downmove, 0, EXCHANGE_ROUND_DECIMALS * 2) > 0)){
						$h[$mkey] = $downmove;
					}
				}
				$p = $h; $i++;
			}
		}

		if ((DEBUG & DEBUG_RATE) && (DEBUG & DEBUG_TRACE_FUNCTIONS_OUTPUT))
			syslog(LOG_INFO|LOG_LOCAL1, "public function rate::dm() = ".print_r($this->historial, true));

		$keys = array($mkey, $pkey);
		return $keys;
	}

	/**
	@param integer
	The number of period for the rsi
	@retval string
	Returns the Gain & Average Gain
	*/
	public function gain($period = 14){
		if ((DEBUG & DEBUG_RATE) && (DEBUG & DEBUG_TRACE_FUNCTIONS))
			syslog(LOG_INFO|LOG_LOCAL1, "private function rate::gain()");

		$agkey = 'average_gain('.$period.')'; $alkey = 'average_loss('.$period.')';
		$t = end($this->historial);

		if (!array_key_exists($agkey, $t) or !array_key_exists($alkey, $t)){
			$i = 1; $k = 1; 
			reset($this->historial);
			foreach ($this->historial as &$h){	// Last element is most the recent

				if ($i < $period){
					$k = $i;
				}
				else{
					$k = $period;
				}
				if ($i == 1){			// First element needs default values
					$h[$alkey] = $h['close'];
					$h[$agkey] = 0;
					$h['loss'] = 0;
					$h['gain'] = 0;
				}
				else{
					$delta = bcsub($h['close'], $p['close'], EXCHANGE_ROUND_DECIMALS * 2);
					if (bccomp($delta, 0, EXCHANGE_ROUND_DECIMALS * 2) < 0){	// Loss
						$h['loss'] = bcabs(number_format($delta, EXCHANGE_ROUND_DECIMALS, '.', ''));
						$h['gain'] = 0;
					}
					else{
						$h['loss'] = 0;
						$h['gain'] = number_format($delta, EXCHANGE_ROUND_DECIMALS, '.', '');
					}
					// Could be smma
					$k1 = $k - 1;
					$h[$alkey] = bcdiv(bcadd(bcmul($p[$alkey], $k1, EXCHANGE_ROUND_DECIMALS * 2), $h['loss'], EXCHANGE_ROUND_DECIMALS * 2), $k, EXCHANGE_ROUND_DECIMALS * 2);
					$h[$agkey] = bcdiv(bcadd(bcmul($p[$agkey], $k1, EXCHANGE_ROUND_DECIMALS * 2), $h['gain'], EXCHANGE_ROUND_DECIMALS * 2), $k, EXCHANGE_ROUND_DECIMALS * 2);
				}
				$p = $h; $i++;
			}
		}
		if ((DEBUG & DEBUG_RATE) && (DEBUG & DEBUG_TRACE_FUNCTIONS_OUTPUT))
			syslog(LOG_INFO|LOG_LOCAL1, "private function rate::gain() = ".print_r($this->historial, true));

		$keys = array('gain','loss',$alkey,$agkey);
		return $keys;
	}

	/**
	@param integer
	The number of period for the rsi
	@retval float
	Returns the RSI
	*/
	public function rsi($period = 14){
		if ((DEBUG & DEBUG_RATE) && (DEBUG & DEBUG_TRACE_FUNCTIONS))
			syslog(LOG_INFO|LOG_LOCAL1, "public function rate::rsi($period)");

		$key = 'rsi('.$period.')';
		$t = end($this->historial);

		if (!array_key_exists($key, $t)){
			$this->gain($period);
			$gain_key = $this->ema($period, 'gain');
			$loss_key = $this->ema($period, 'loss');

			reset($this->historial);
			foreach ($this->historial as &$h){	// Last element is most rescent
				if (bccomp($h[$loss_key], 0, EXCHANGE_ROUND_DECIMALS * 2) > 0){
					$rs = bcdiv($h[$gain_key], $h[$loss_key], EXCHANGE_ROUND_DECIMALS * 2);
					//RSI = (100 – (100 / (1 + RS)))
					$h[$key] = bcsub(100, bcdiv(bcconv(100), bcadd(bcconv(1), bcconv($rs), EXCHANGE_ROUND_DECIMALS * 2), EXCHANGE_ROUND_DECIMALS * 2), 2);
				}
				else{
					$h[$key] = 100;
				}
			}
		}

		if ((DEBUG & DEBUG_RATE) && (DEBUG & DEBUG_TRACE_FUNCTIONS_OUTPUT))
			syslog(LOG_INFO|LOG_LOCAL1, "public function rate::rsi($period) = ".print_r($this->historial, true));

		return $key;
	}

	/**
	@param integer
	The number of the short period
	@param integer
	The number of the long period
	*/
	public function macd($short_period = 12, $long_period = 26, $signal_period = 9){
		if ((DEBUG & DEBUG_RATE) && (DEBUG & DEBUG_TRACE_FUNCTIONS))
			syslog(LOG_INFO|LOG_LOCAL1, "public function rate::macd($short_period, $long_period, $signal_period)");

		$t = end($this->historial);
		$macdkey = 'macd('.$short_period.','.$long_period.','.$signal_period.')';
		$sigkey = 'signal('.$macdkey.')';
		if (!array_key_exists($macdkey, $t)){
			$skey = $this->ema($short_period, 'close'); // $skey = 'ema('.$short_period.',close)';
			$lkey = $this->ema($long_period, 'close'); // $lkey = 'ema('.$long_period.',close)';

			reset($this->historial);
			foreach ($this->historial as &$h){
				$h[$macdkey] = bcsub($h[$skey], $h[$lkey], EXCHANGE_ROUND_DECIMALS * 2);
			}
		}

		$emasigkey = $this->ema($signal_period, $macdkey); // $emasigkey = 'ema('.$signal_period.','.$macdkey.')';
		$this->rename_key($emasigkey, $sigkey);
		$dkey = 'delta('.$macdkey.','.$sigkey.')';

		if (!array_key_exists($dkey, $t)){
			reset($this->historial);
			foreach ($this->historial as &$h){
				$h[$dkey] = bcsub($h[$macdkey], $h[$sigkey], EXCHANGE_ROUND_DECIMALS * 2);
			}
		}
		$keys = array($macdkey, $sigkey, $dkey);
		return $keys;
	}

	/**
	@param integer
	The number of period for the atr
	@retval string
	Returns the ATR
	*/
	public function atr($period = 14){
		if ((DEBUG & DEBUG_RATE) && (DEBUG & DEBUG_TRACE_FUNCTIONS))
			syslog(LOG_INFO|LOG_LOCAL1, "public function rate::atr($period)");

		$t = end($this->historial);
		$key = 'atr('.$period.')';

		if (!array_key_exists($key, $t)){
			$tr_key = $this->tr();
			$okey = $this->ema($period,$tr_key); // $okey = 'ema('.$period.',tr)';

			// Rename ema(period,tr) new key into atr one
			$this->rename_key($okey, $key);
		}

		if ((DEBUG & DEBUG_RATE) && (DEBUG & DEBUG_TRACE_FUNCTIONS_OUTPUT))
			syslog(LOG_INFO|LOG_LOCAL1, "public function rate::atr($period) = ".print_r($this->historial, true));
		return $key;
	}

	/**
	@param integer
	The number of period for the atr
	@retval string
	Returns the ATRP
	*/
	public function atrp($period = 14){
		if ((DEBUG & DEBUG_RATE) && (DEBUG & DEBUG_TRACE_FUNCTIONS))
			syslog(LOG_INFO|LOG_LOCAL1, "public function rate::atrp($period)");

		$t = end($this->historial);
		$key = 'atrp('.$period.')';

		if (!array_key_exists($key, $t)){
			$atr_key = $this->atr($period);
			reset($this->historial);
			foreach ($this->historial as &$h){
				$h[$key] = bcmul(bcdiv($h[$atr_key], $h['close'], EXCHANGE_ROUND_DECIMALS * 2), 100, 2);
			}
		}

		if ((DEBUG & DEBUG_RATE) && (DEBUG & DEBUG_TRACE_FUNCTIONS_OUTPUT))
			syslog(LOG_INFO|LOG_LOCAL1, "public function rate::atrp($period) = ".print_r($this->historial, true));
		return $key;
	}
	/**
	@param integer
	The number of period for the ADX
	@retval float
	Returns the ADX
	*/
	public function adx($period = 14){
		if ((DEBUG & DEBUG_RATE) && (DEBUG & DEBUG_TRACE_FUNCTIONS))
			syslog(LOG_INFO|LOG_LOCAL1, "public function rate::adx($period)");

		$adx_key = 'adx('.$period.')';
		$dx_key = 'dx()';
		$pkey = '+di('.$period.')';
		$mkey = '-di('.$period.')';
		$t = end($this->historial);
		if (!array_key_exists($mkey, $t) or !array_key_exists($pkey, $t) or !array_key_exists($adx_key, $t) or !array_key_exists($dx_key, $t)){
			$tr_key = $this->tr();
			list($minus_dm_key, $plus_dm_key) = $this->dm();
			$sma_dmp_key = $this->sma($period, $plus_dm_key); // $sma_dmp_key = 'sma('.$period.',+dm)';
			$sma_dmm_key = $this->sma($period, $minus_dm_key); // $sma_dmm_key = 'sma('.$period.',-dm)';
			$sma_tr_key = $this->sma($period, $tr_key); // $sma_tr_key = 'sma('.$period.',tr)';
			$tpkey = "t$pkey"; $tmkey = "t$mkey";

			reset($this->historial);
			foreach ($this->historial as &$h){
				$tp = bcabs(bcdiv($h[$sma_dmp_key], $h[$sma_tr_key], EXCHANGE_ROUND_DECIMALS * 2));
				$tm = bcabs(bcdiv($h[$sma_dmm_key], $h[$sma_tr_key], EXCHANGE_ROUND_DECIMALS * 2));
				$h[$pkey] = bcmul(100, $tp, 2);
				$h[$mkey] = bcmul(100, $tm, 2);

				$sub = bcsub($h[$pkey], $h[$mkey], EXCHANGE_ROUND_DECIMALS * 2);
				$add = bcadd($h[$pkey], $h[$mkey], EXCHANGE_ROUND_DECIMALS * 2);
				if (bccomp($add, 0, EXCHANGE_ROUND_DECIMALS * 2)){
					$h[$dx_key] = bcmul(100, bcabs(bcdiv($sub, $add, EXCHANGE_ROUND_DECIMALS * 2)), 2);
				}
				else{
					$h[$dx_key] = 0;
				}
			}
			$ttkey = $this->smma($period, $dx_key); // $ttkey = 'ema('.$period.','.$tkey.')';
			$this->rename_key($ttkey, $adx_key);

			reset($this->historial);
			foreach ($this->historial as &$h){
				$h[$adx_key] = number_format($h[$adx_key], 2, '.', '');
			}
		}
		if ((DEBUG & DEBUG_RATE) && (DEBUG & DEBUG_TRACE_FUNCTIONS_OUTPUT))
			syslog(LOG_INFO|LOG_LOCAL1, "public function rate::adx($period) = ".print_r($this->historial, true));
		$keys = array($adx_key, $dx_key, $mkey, $pkey);
		return $keys;
	}

	/**
	@param integer
	The number of period for the Smoothing Moving Average
	@param string
	Key to use
    // https://mahifx.com/mfxtrade/indicators/smoothed-moving-average-smma
    // The first value for the Smoothed Moving Average is calculated as a Simple Moving Average (SMA):
    //
    // SUM1=SUM (CLOSE, N)
    //
    // SMMA1 = SUM1/ N
    //
    // The second and subsequent moving averages are calculated according to this formula:
    //
    // SMMA (i) = (SUM1 – SMMA1+CLOSE (i))/ N
    //
    // Where:
    //
    // SUM1 – is the total sum of closing prices for N periods;
    // SMMA1 – is the smoothed moving average of the first bar;
    // SMMA (i) – is the smoothed moving average of the current bar (except the first one);
    // CLOSE (i) – is the current closing price;
    // N – is the smoothing period.
	*/
	public function smma($period = 20, $index = 'close'){
		if ((DEBUG & DEBUG_RATE) && (DEBUG & DEBUG_TRACE_FUNCTIONS))
			syslog(LOG_INFO|LOG_LOCAL1, "public function rate::smma($period, $index)");
		$key = 'smma('.$period.','.$index.')';
		$t = end($this->historial);

		if (!array_key_exists($key, $t)){
			$sma_key = $this->sma($period, $index);
			$i = 0;
			$k = $period - 1;
			reset($this->historial);
			foreach ($this->historial as &$h){	// Last element is the most rescent
				if ($i == 0){
					$h[$key] = $h[$sma_key];
					$i = 1;
				}
				else{
					$h[$key] = bcdiv(bcadd(bcmul($p[$key], $k, EXCHANGE_ROUND_DECIMALS * 2), $h[$index], EXCHANGE_ROUND_DECIMALS * 2), $period, EXCHANGE_ROUND_DECIMALS * 2);
				}
				$p = $h;
			}
		}

		return $key;
	}


	/**
	@param integer
	The number of period for the Simple Moving Average
	@param string
	Key to use
	*/
	public function sma($period = 20, $index = 'close'){
		if ((DEBUG & DEBUG_RATE) && (DEBUG & DEBUG_TRACE_FUNCTIONS))
			syslog(LOG_INFO|LOG_LOCAL1, "public function rate::sma($period, $index)"); 

		$key = 'sma('.$period.','.$index.')';
		$t = end($this->historial);

		if (!array_key_exists($key, $t)){
			$buffer = array();
			reset($this->historial);
			foreach ($this->historial as &$h){	// Last element is the most rescent
				array_push($buffer, $h[$index]);
				if (count($buffer) > $period){
					array_shift($buffer);
				}
				$sum = 0;
				foreach ($buffer as $b){
					$sum = bcadd(bcconv($sum), $b, EXCHANGE_ROUND_DECIMALS * 2);
				}
				$h[$key] = bcdiv($sum, bcconv($period), EXCHANGE_ROUND_DECIMALS * 2);
			}
		}

		if ((DEBUG & DEBUG_RATE) && (DEBUG & DEBUG_TRACE_FUNCTIONS_OUTPUT))
			syslog(LOG_INFO|LOG_LOCAL1, "public function rate::sma($period, $index) = ".print_r($this->historial, true));
		return $key;
	}

	public function midkey($key1 = 'high', $key2 = 'low'){
		if ((DEBUG & DEBUG_RATE) && (DEBUG & DEBUG_TRACE_FUNCTIONS))
			syslog(LOG_INFO|LOG_LOCAL1, "public function rate::midkey($key1, $key2)");

		$t = end($this->historial);
		$key = 'average('.$key1.','.$key2.')';
		if (!array_key_exists($key, $t)){
			reset($this->historial);
			foreach ($this->historial as &$h){	// Last element is the most rescent
					$h[$key] = bcdiv(bcadd($h[$key1], $h[$key2], EXCHANGE_ROUND_DECIMALS * 2), 2, EXCHANGE_ROUND_DECIMALS * 2);
				}
		}

		return $key;
	}

	/**
	@param integer
	The number of short period for the Awesome Oscillator
	@param integer
	The number of long period for the Awesome Oscillator
	*/
	public function ao($short = 5, $long = 34){
		if ((DEBUG & DEBUG_RATE) && (DEBUG & DEBUG_TRACE_FUNCTIONS))
			syslog(LOG_INFO|LOG_LOCAL1, "public function rate::ao($short, $long)"); 

		$t = end($this->historial);
		$key = 'ao('.$short.','.$long.')';
		$key_color = 'ao_color('.$short.','.$long.')';
		$midkey = $this->midkey();
		if (!array_key_exists($key, $t) or !array_key_exists($midkey, $t) or !array_key_exists($key_color, $t)){

			$sma_short_key = $this->sma($short, $midkey);
			$sma_long_key = $this->sma($long, $midkey);

			$i = 1;
			reset($this->historial);
			foreach ($this->historial as &$h){	// Last element is the most rescent
				$h[$key] = bcsub($h[$sma_short_key], $h[$sma_long_key], EXCHANGE_ROUND_DECIMALS * 2);
				if ($i == 1){
					$h[$key_color] = 'green';
				}
				else{
					if (bccomp($h[$key], $p[$key], EXCHANGE_ROUND_DECIMALS * 2) > -1){
						$h[$key_color] = 'green';
					}
					else{
						$h[$key_color] = 'red';
					}
				}
				$p = $h; $i++;
			}
		}

		$keys = array($key, $key_color, $midkey);
		return $keys;
	}

	/**
	@param integer
	The number of period for the Acellerator
	*/
	public function ac($period = 5){
	if ((DEBUG & DEBUG_RATE) && (DEBUG & DEBUG_TRACE_FUNCTIONS))
			syslog(LOG_INFO|LOG_LOCAL1, "public function rate::ac($period)");

		$key = 'ac('.$period.')';
		$t = end($this->historial);

		if (!array_key_exists($key, $t)){
			list($key_ao, $key_ao_color, $midkey) = $this->ao($period, 34);
			$smakey = $this->sma($period, $key_ao);
			reset($this->historial);
			foreach ($this->historial as &$h){	// Last element is the most rescent
				$h[$key] = bcsub($h[$key_ao], $h[$smakey], EXCHANGE_ROUND_DECIMALS * 2);
			}
		}

		return $key;
	}

	/**
	@param integer
	The number of period for the Bollinger Band
	@param integer
	The number of standard deviation for the Bollinger Bad
	*/
	public function bb($period = 20, $stddev = 2){
		if ((DEBUG & DEBUG_RATE) && (DEBUG & DEBUG_TRACE_FUNCTIONS))
			syslog(LOG_INFO|LOG_LOCAL1, "public function rate::bb($period, $stddev)");

		$t = end($this->historial);
		$keyhbb = 'bb_high('.$period.','.$stddev.')';
		$keylbb = 'bb_low('.$period.','.$stddev.')';
		$bb_qz = 'bb_bw('.$period.','.$stddev.')';
		$keystd = 'stddev('.$period.')';
		$sma_key = $this->sma($period, 'close');
		if (!array_key_exists($keyhbb, $t) or !array_key_exists($keylbb, $t) or !array_key_exists($bb_qz, $t) or !array_key_exists($keystd, $t)){
			$buffer = array(); $i = 0;
			reset($this->historial);
			foreach ($this->historial as &$h){	// Last element is the most rescent
				array_push($buffer, $h['close']);
				if (count($buffer) > $period){
					array_shift($buffer);
				}
				if (count($buffer) > 1){
					$std = stats_standard_deviation($buffer, true);
					$h[$keystd] = $std;
					$h[$keyhbb] = bcadd($h[$sma_key], bcmul($stddev, $std, EXCHANGE_ROUND_DECIMALS * 2), EXCHANGE_ROUND_DECIMALS * 2);
					$h[$keylbb] = bcsub($h[$sma_key], bcmul($stddev, $std, EXCHANGE_ROUND_DECIMALS * 2), EXCHANGE_ROUND_DECIMALS * 2);
					$h[$bb_qz] = bcdiv(bcsub($h[$keyhbb], $h[$keylbb], EXCHANGE_ROUND_DECIMALS * 2), $h[$sma_key], EXCHANGE_ROUND_DECIMALS * 2);
				}
			}
		}

		if ((DEBUG & DEBUG_RATE) && (DEBUG & DEBUG_TRACE_FUNCTIONS_OUTPUT))
			syslog(LOG_INFO|LOG_LOCAL1, "public function rate::bb($period, $stddev) = ".print_r($this->historial, true));
		$keys = array($keylbb, $keyhbb, $bb_qz, $keystd, $sma_key);
		return $keys;
	}

	/**
	@param integer
	The number of period for the Keltner band
	@param integer
	The number of ATR desviation
	*/
	public function keltner($period = 20, $bandwidth = 1.5){
		if ((DEBUG & DEBUG_RATE) && (DEBUG & DEBUG_TRACE_FUNCTIONS))
			syslog(LOG_INFO|LOG_LOCAL1, "public function rate::keltner($period, $bandwidth)");

		$t = end($this->historial);
		$keyhk = 'keltner_high('.$period.','.$bandwidth.')';
		$keylk = 'keltner_low('.$period.','.$bandwidth.')';
		$key_tp = $this->tp();
		$sma_key = $this->sma($period, $key_tp);
		if (!array_key_exists($keyhk, $t) or !array_key_exists($keylk, $t)){
			$key_atr = $this->atr(14);
			reset($this->historial);
			foreach ($this->historial as &$h){	// Last element is the most rescent
				$h[$keyhk] = bcadd($h[$sma_key], bcmul($bandwidth, $h[$key_atr], EXCHANGE_ROUND_DECIMALS * 2), EXCHANGE_ROUND_DECIMALS * 2);
				$h[$keylk] = bcsub($h[$sma_key], bcmul($bandwidth, $h[$key_atr], EXCHANGE_ROUND_DECIMALS * 2), EXCHANGE_ROUND_DECIMALS * 2);
			}
		}

		if ((DEBUG & DEBUG_RATE) && (DEBUG & DEBUG_TRACE_FUNCTIONS_OUTPUT))
			syslog(LOG_INFO|LOG_LOCAL1, "public function rate::keltner($period, $bandwidth) = ".print_r($this->historial, true));
		$keys = array($keylk, $keyhk, $sma_key);
		return $keys;
	}

	public function squeeze(){
		if ((DEBUG & DEBUG_RATE) && (DEBUG & DEBUG_TRACE_FUNCTIONS))
			syslog(LOG_INFO|LOG_LOCAL1, "public function rate::squeeze()");

		$t = end($this->historial);
		$key = 'squeeze()';
		$key_choppy = 'choppy()';
		if (!array_key_exists($key, $t) or !array_key_exists($key_choppy, $t)){
			list($key_lbb_20_2, $key_hbb_20_2, $key_bb_qz_20_2, $key_std_20_2, $key_sma_20_2) = $this->bb(20, 2);
			list($key_lk, $key_hk, $key_sma_close) = $this->keltner(20, 1.5);
			reset($this->historial);
			foreach ($this->historial as &$h){	// Last element is the most rescent
				$h[$key] = bcsub($h[$key_hbb_20_2], $h[$key_hk], EXCHANGE_ROUND_DECIMALS * 2);
				if (bccomp(bcconv($h[$key]), bcconv('0.0'), EXCHANGE_ROUND_DECIMALS) > 0){
					$h[$key_choppy] = 0;
				}
				else{
					$h[$key_choppy] = 1;
				}
			}
		}
		if ((DEBUG & DEBUG_RATE) && (DEBUG & DEBUG_TRACE_FUNCTIONS_OUTPUT))
			syslog(LOG_INFO|LOG_LOCAL1, "public function rate::squeeze() = ".print_r($this->historial, true));
		$keys = array($key, $key_choppy);
		return $keys;
	}

	public function compare($index = 'close', $compare = 'open', $digits = (EXCHANGE_ROUND_DECIMALS * 2)){
		if ((DEBUG & DEBUG_RATE) && (DEBUG & DEBUG_TRACE_FUNCTIONS))
			syslog(LOG_INFO|LOG_LOCAL1, "public function rate::compare($index, $compare)");

		$key = 'compare('.$index.','.$compare.')';
		$t = end($this->historial);
		if (!array_key_exists($key, $t)){
			reset($this->historial);
			foreach ($this->historial as &$h){	// Last element is the most rescent
				$h[$key] = bccomp(bcconv($h[$index]), bcconv($h[$compare]), $digits);
			}
		}

		if ((DEBUG & DEBUG_RATE) && (DEBUG & DEBUG_TRACE_FUNCTIONS_OUTPUT))
			syslog(LOG_INFO|LOG_LOCAL1, "public function rate::compare($index, $compare) = ".print_r($this->historial, true));
		return $key;
	}

	public function compare_referal($index = 'close', $compare = 'open', $digits = (EXCHANGE_ROUND_DECIMALS * 2)){
		if ((DEBUG & DEBUG_RATE) && (DEBUG & DEBUG_TRACE_FUNCTIONS))
			syslog(LOG_INFO|LOG_LOCAL1, "public function rate::compare_referal($index, $compare)");

		$key = 'compare_referal('.$index.','.$compare.')';
		$t = end($this->historial);
		if (!array_key_exists($key, $t)){
			reset($this->historial);
			foreach ($this->historial as &$h){	// Last element is the most rescent
				$c = $h[$compare];
				if (array_key_exists($c, $h)){
					$h[$key] = bccomp(bcconv($h[$index]), bcconv($h[$c]), $digits);
				}
				else{
//					$h[$key] = bccomp(bcconv($h[$index]), bcconv($h[$compare]), $digits);
					$h[$key] = null;
				}
			}
		}

		if ((DEBUG & DEBUG_RATE) && (DEBUG & DEBUG_TRACE_FUNCTIONS_OUTPUT))
			syslog(LOG_INFO|LOG_LOCAL1, "public function rate::compare_referal($index, $compare) = ".print_r($this->historial, true));
		return $key;
	}

	public function compare_keys($index = 'close', $compare = 'open', $digits = (EXCHANGE_ROUND_DECIMALS * 2), $inverse = false){
		if ((DEBUG & DEBUG_RATE) && (DEBUG & DEBUG_TRACE_FUNCTIONS))
			syslog(LOG_INFO|LOG_LOCAL1, "public function rate::compare_keys($index, $compare)");

		$key = 'compare_keys'.($inverse?'_inverse':'').'('.$index.','.$compare.')';
		$t = end($this->historial);
		if (!array_key_exists($key, $t)){
			reset($this->historial);
			foreach ($this->historial as &$h){      // Last element is the most rescent
				$u = bccomp(bcconv($h[$index]), bcconv($h[$compare]), $digits);
				if (1 == $u){
					$h[$key] = $inverse?$compare:$index;
				}
				elseif (-1 == $u){
					$h[$key] = $inverse?$index:$compare;
				}
				else{
					$h[$key] = null;
				}
			}
		}

		if ((DEBUG & DEBUG_RATE) && (DEBUG & DEBUG_TRACE_FUNCTIONS_OUTPUT))
			syslog(LOG_INFO|LOG_LOCAL1, "public function rate::compare_keys($index, $compare) = ".print_r($this->historial, true));
		return $key;
	}

	public function compare_constant($index = 'close', $compare = 0){
		if ((DEBUG & DEBUG_RATE) && (DEBUG & DEBUG_TRACE_FUNCTIONS))
			syslog(LOG_INFO|LOG_LOCAL1, "public function rate::compare($index)");

		$key = 'compare_constant('.$index.','.$compare.')';
		$t = end($this->historial);
		if (!array_key_exists($key, $t)){
			reset($this->historial);
			foreach ($this->historial as &$h){	// Last element is the most rescent
				$h[$key] = bccomp(bcconv($h[$index]), bcconv($compare), EXCHANGE_ROUND_DECIMALS * 2);
			}
		}

		if ((DEBUG & DEBUG_RATE) && (DEBUG & DEBUG_TRACE_FUNCTIONS_OUTPUT))
			syslog(LOG_INFO|LOG_LOCAL1, "public function rate::compare($index) = ".print_r($this->historial, true));
		return $key;
	}

	/**
	@param string
	The number of period for the Simple Moving Average
	@param integer
	Key to use
	*/
	public function greater_smaller($period = 20, $index = 'close', $compare = 'open'){
		if ((DEBUG & DEBUG_RATE) && (DEBUG & DEBUG_TRACE_FUNCTIONS))
			syslog(LOG_INFO|LOG_LOCAL1, "public function rate::greater_smaller($period, $index, $compare)");

		$greater_key = 'greater('.$period.','.$index.','.$compare.')';
		$smaller_key = 'smaller('.$period.','.$index.','.$compare.')';
		// We are not using rate::compare() to save cycles
		$compare_key = 'compare('.$index.','.$compare.')';
		$t = end($this->historial);

		if (!array_key_exists($greater_key, $t) or !array_key_exists($smaller_key, $t) or !array_key_exists($compare_key, $t)){
			$buffer = array();

			reset($this->historial);
			foreach ($this->historial as &$h){	// Last element is the most rescent
				$h[$compare_key] = bccomp(bcconv($h[$index]), bcconv($h[$compare]), EXCHANGE_ROUND_DECIMALS * 2);
				array_push($buffer, $h[$compare_key]);

				if (count($buffer) > $period){
					array_shift($buffer);
				}

				$h[$smaller_key] = 0;
				$h[$greater_key] = 0;

				if (count($buffer)){
					$array_count = array_count_values($buffer);
					if (array_key_exists('-1', $array_count)){
						$h[$smaller_key] += $array_count['-1'];
					}

					if (array_key_exists('0', $array_count)){
						$h[$greater_key] += $array_count['0'];
					}

					if (array_key_exists('1', $array_count)){
						$h[$greater_key] += $array_count['1'];
					}
				}
			}
		}

		$keys = array($compare_key, $smaller_key, $greater_key);
		return $keys;
	}

	public function sto($period1 = 14, $period2 = 3, $period3 = 3){
		if ((DEBUG & DEBUG_RATE) && (DEBUG & DEBUG_TRACE_FUNCTIONS))
			syslog(LOG_INFO|LOG_LOCAL1, "public function rate::sto($period1 = 14, $period2 = 3, $period3 = 3)");

		$key_fastk = '%k('.$period1.')';
		$key_dk = '%dk('.$period1.','.$period2.')';
		$key_slowd = '%d('.$period1.','.$period2.','.$period3.')';
		$t = end($this->historial);
		if (!array_key_exists($key_fastk, $t) or !array_key_exists($key_dk, $t) or !array_key_exists($key_slowd, $t)){
			list($key_min_max_high_min, $key_min_max_high_max, $key_min_max_high_steps_min, $key_min_max_high_steps_max, $key_abs_min_max_high_min, $key_abs_min_max_high_max, $key_abs_min_max_high_steps_min, $key_abs_min_max_high_steps_max) = $this->min_max(14, 'high', EXCHANGE_ROUND_DECIMALS);
			list($key_min_max_low_min, $key_min_max_low_max, $key_min_max_low_steps_min, $key_min_max_low_steps_max, $key_abs_min_max_low_min, $key_abs_min_max_low_max, $key_abs_min_max_low_steps_min, $key_abs_min_max_low_steps_max) = $this->min_max(14, 'low', EXCHANGE_ROUND_DECIMALS);

			reset($this->historial);
			foreach ($this->historial as &$h){	// Last element is the most rescent
				$h[$key_fastk] = bcmul(100, bcdiv(bcsub($h['close'], $h[$key_min_max_low_min], EXCHANGE_ROUND_DECIMALS), bcsub($h[$key_min_max_high_max], $h[$key_min_max_low_min], EXCHANGE_ROUND_DECIMALS), EXCHANGE_ROUND_DECIMALS), 2);
			}
			$t = $this->sma($period2, $key_fastk);
			$this->rename_key($t, $key_dk);
			$s = $this->sma($period3, $key_dk);
			$this->rename_key($s, $key_slowd);
			reset($this->historial);
			foreach ($this->historial as &$h){	// Last element is the most rescent
				$h[$key_dk] = number_format($h[$key_dk], 2, '.', '');
				$h[$key_slowd] = number_format($h[$key_slowd], 2, '.', '');
			}
		}

		$keys = array($key_fastk, $key_dk, $key_slowd);
		return $keys;
	}

	public function delayed($period = 26, $index = 'close'){
	        if ((DEBUG & DEBUG_RATE) && (DEBUG & DEBUG_TRACE_FUNCTIONS))
			syslog(LOG_INFO|LOG_LOCAL1, "public function rate::delayed($period, $index)");

		$key = 'delayed('.$period.','.$index.')';
		$t = end($this->historial);
		if (!array_key_exists($key, $t)){
			$i = 0; $buffer = array();
			reset($this->historial);
            		foreach ($this->historial as &$h){	// Last element is the most rescent
				array_unshift($buffer, $h[$index]);	// First element is the most rescent
				if ($i == 0)
					$vz = $h[$index];
				if (count($buffer) > $period){
					$v = array_pop($buffer);
				}
				else {
					$v = $vz;
				}
				$h[$key] = $v;
				$i++;
			}
		}
		return $key;
	}

	/**
	@param integer
	The number of period for the Simple Moving Average
	@param string
	Key to use
	*/
	public function min_max($period = 30, $index = 'close', $decimals = EXCHANGE_ROUND_DECIMALS){

		if ((DEBUG & DEBUG_RATE) && (DEBUG & DEBUG_TRACE_FUNCTIONS))
			syslog(LOG_INFO|LOG_LOCAL1, "public function rate::min_max($period, $index, $decimals)");

		//$c = round(count($this->historial)/2, 0);
		$minkey = 'min('.$period.','.$index.','.$decimals.')';
		$maxkey = 'max('.$period.','.$index.','.$decimals.')';
		$absminkey = 'absmin('.$index.','.$decimals.')';
		$absmaxkey = 'absmax('.$index.','.$decimals.')';

		$stepsminkey = 'steps('.$minkey.')';
		$stepsmaxkey = 'steps('.$maxkey.')';
		$absstepsminkey = 'abssteps('.$absminkey.')';
		$absstepsmaxkey = 'abssteps('.$absmaxkey.')';
		$t = end($this->historial);
		if (!array_key_exists($minkey, $t) or !array_key_exists($maxkey, $t) or !array_key_exists($stepsminkey, $t) or !array_key_exists($stepsmaxkey, $t) or !array_key_exists($absminkey, $t) or !array_key_exists($absmaxkey, $t) or !array_key_exists($absstepsminkey, $t) or !array_key_exists($absstepsmaxkey, $t)) {
			$i = 0; $buffer = array();
			reset($this->historial);
			foreach ($this->historial as &$h){	// Last element is the most rescent
				array_unshift($buffer, $h);	// First element is the most rescent

				if ($i == 0) {
					$h[$maxkey] = $h[$index];		// You are always the max 
					$h[$minkey] = $h[$index];		// You are always the min
					$h[$absmaxkey] = $h[$index];		// You are always the max 
					$h[$absminkey] = $h[$index];		// You are always the min
					$h[$stepsmaxkey] = 0;
					$h[$stepsminkey] = 0;
					$h[$absstepsmaxkey] = 0;
					$h[$absstepsminkey] = 0;
				}
				else{
					bcscale($decimals);
					$h[$absmaxkey] = bcmax($p[$absmaxkey], $h[$index]);
					$h[$absminkey] = bcmin($p[$absminkey], $h[$index]);

					if (count($buffer) > $period){
						array_pop($buffer);
					}

					reset($buffer);
					$h[$maxkey] = current($buffer)[$index];
					$h[$minkey] = current($buffer)[$index];

					foreach ($buffer as $b){
						$h[$maxkey] = bcmax($b[$index], $h[$maxkey]);
						$h[$minkey] = bcmin($b[$index], $h[$minkey]);
					}

					//$h[$minkey] = number_format(bcmin($h[$index], $p[$minkey]), $decimals, '.', '');
					//$h[$maxkey] = number_format(bcmax($h[$index], $p[$maxkey]), $decimals, '.', '');
					bcscale(EXCHANGE_ROUND_DECIMALS);

					$h[$stepsmaxkey] = 0;
					$h[$stepsminkey] = 0;
					$h[$absstepsmaxkey] = 0;
					$h[$absstepsminkey] = 0;

					// Look for the absolute
					if (bccomp($h[$index], $h[$maxkey], $decimals) == -1){
						$h[$absstepsmaxkey] = $p[$absstepsmaxkey] + 1;
					}

					if (bccomp($h[$index], $h[$minkey], $decimals) == 1){
						$h[$absstepsminkey] = $p[$absstepsminkey] + 1;
					}

					//print_r($buffer);
					// Look for the relative
					for ($ii = 1, $found = false; ($ii < count($buffer)) ; $ii++){
						//echo 'MAX '.$ii.' comparing '. $h[$maxkey] . ' vs '.$buffer[$ii][$index].PHP_EOL;
						if ((bccomp($h[$maxkey], $buffer[$ii][$index], $decimals) == 0) && !$found){
							//echo 'is '.$ii.PHP_EOL;
							$h[$stepsmaxkey] = $ii;
							$found = true;
						}
					}

					for ($jj = 1, $found = false; ($jj < count($buffer)) ; $jj++){
						//echo 'MIN '.$jj.' comparing '. $h[$minkey] . ' vs '.$buffer[$jj][$index].PHP_EOL;
						if ((bccomp($h[$minkey], $buffer[$jj][$index], $decimals) == 0) && !$found){
							//echo 'is '.$jj.PHP_EOL;
							$h[$stepsminkey] = $jj;
							$found = true;
						}
					}
				}
				$i++; $p = $h;
			}
		}
		if ((DEBUG & DEBUG_RATE) && (DEBUG & DEBUG_TRACE_FUNCTIONS_OUTPUT))
			syslog(LOG_INFO|LOG_LOCAL1, "public function rate::min_max($period, $index, $decimals) = ".print_r($this->historial, true));

		$keys = array($minkey, $maxkey, $stepsminkey, $stepsmaxkey, $absminkey, $absmaxkey, $absstepsminkey, $absstepsmaxkey);
		return $keys;
	}

	/**
	@param string
	The number of period for the atr
	@param string
	@retval string
	Returns the ATR
	*/
	public function percentage($index1 = 'close', $index2 = 'open'){
		if ((DEBUG & DEBUG_RATE) && (DEBUG & DEBUG_TRACE_FUNCTIONS))
			syslog(LOG_INFO|LOG_LOCAL1, "public function rate::percentage($index1, $index2)");

		$t = end($this->historial);
		$key = 'percentage('.$index1.','.$index2.')';

		if (!array_key_exists($key, $t)){
			reset($this->historial);
			foreach ($this->historial as &$h){
				$h[$key] = bcmul(bcdiv(bcsub($h[$index1], $h[$index2], EXCHANGE_ROUND_DECIMALS * 2), $h[$index1], EXCHANGE_ROUND_DECIMALS * 2), 100, 2);
			}
		}

		if ((DEBUG & DEBUG_RATE) && (DEBUG & DEBUG_TRACE_FUNCTIONS_OUTPUT))
			syslog(LOG_INFO|LOG_LOCAL1, "public function rate::percentage($index1, $index2) = ".print_r($this->historial, true));
		return $key;
	}

	public function consecutive($index = 'close', $precision = EXCHANGE_ROUND_DECIMALS){
		if ((DEBUG & DEBUG_RATE) && (DEBUG & DEBUG_TRACE_FUNCTIONS))
			syslog(LOG_INFO|LOG_LOCAL1, "public function rate::consecutive($index)");

		$t = end($this->historial);
		$key = 'consecutive('.$index.')';

		if (!array_key_exists($key, $t)){
			$i = 0;
			reset($this->historial);
			foreach ($this->historial as &$h){
				if ($i == 0){
					$i = 1;
					$h[$key] = 0;
				}
				else{
					if (is_numeric($h[$index])){
						if (bccomp($p[$index], $h[$index], $precision) == 0){
							$h[$key] = $p[$key] + 1;
						}
						else{
							$h[$key] = 0;
						}
					}
					else{
						if (strcasecmp($p[$index], $h[$index]) == 0){
							$h[$key] = $p[$key] + 1;
						}
						else{
							$h[$key] = 0;
						}
					}
				}
				$p = $h;
			}
		}

		if ((DEBUG & DEBUG_RATE) && (DEBUG & DEBUG_TRACE_FUNCTIONS_OUTPUT))
			syslog(LOG_INFO|LOG_LOCAL1, "public function rate::consecutive($index) = ".print_r($this->historial, true));
		return $key;
	}

	public function inflexion($keyA = 'close',$keyB = 'ema(2,close)'){
		if ((DEBUG & DEBUG_RATE) && (DEBUG & DEBUG_TRACE_FUNCTIONS))
			syslog(LOG_INFO|LOG_LOCAL1, "public function rate::inflexion($keyA, $keyB)");

		$t = end($this->historial);
		$key = null;

		if (array_key_exists($keyA, $t) && array_key_exists($keyB, $t)){
			$key = 'inflexion('.$keyA.','.$keyB.')';

			if (!array_key_exists($key, $t)){
				list($key_fastk, $key_dk, $key_slowd) = $this->sto(14, 3, 3);
				$i = 0;
				$key_compare = $this->compare($keyA, $keyB, EXCHANGE_ROUND_DECIMALS);
				$key_consecutive_compare = $this->consecutive($key_compare);
				list($key_tenkansen, $key_kijunsen, $key_chikou, $key_senkou_a, $key_senkou_b) = $this->ichimoku();
				list($key_kijunsen_slope, $key_kijunsen_slope_sign) = $this->slope($key_kijunsen, 1);

				foreach ($this->historial as &$h){
					$h[$key] = 0;
					if ($h[$key_consecutive_compare] == 0){ // Inflexion point at $i - 1
						if ($i){
							$k = $i - 1;
							$fast_test = ($this->historial[$k][$key_fastk] + $this->historial[$k][$key_dk])/2;
							$slow_test = ($this->historial[$k][$key_dk] + $this->historial[$k][$key_slowd])/2;
							$k_compare = $this->historial[$k][$key_compare];
							if (	(($fast_test >= 80) && ($k_compare == 1)) ||
								(($fast_test <= 20) && ($k_compare == -1)) ||
								(($slow_test >= 70) && ($k_compare == 1)) ||
								(($slow_test <= 30) && ($k_compare == -1))
							)
							$this->historial[$k][$key] = 1;
						}
					}
					$i++;
				}

				foreach ($this->historial as &$h){
					if ($h[$key_kijunsen_slope_sign] == 0)
						$this->historial[$k][$key] = 0;
				}
			}
		}

		if ((DEBUG & DEBUG_RATE) && (DEBUG & DEBUG_TRACE_FUNCTIONS_OUTPUT))
			syslog(LOG_INFO|LOG_LOCAL1, "public function rate::inflexion($keyA, $keyB) = ".print_r($this->historial, true));

		return $key;
	}

	public function detect_trending(){
		if ((DEBUG & DEBUG_RATE) && (DEBUG & DEBUG_TRACE_FUNCTIONS))
			syslog(LOG_INFO|LOG_LOCAL1, "public function rate::detect_trending()");
		$t = end($this->historial);
		$key = 'trend()';
		$key_up = 'uptrend()';
		$key_down = 'downtrend()';
		if (!array_key_exists($key, $t) || !array_key_exists($key_up, $t) || !array_key_exists($key_down, $t)){
			$key_sma_close = $this->sma(20, 'close');
			list($compare_key, $smaller_key, $greater_key) = $this->greater_smaller(20, 'close', $key_sma_close);
			list($key_squeeze, $key_choppy) = $this->squeeze();
			$key_chop14 = $this->chop(14);
			$key_compare_close_sma = $this->compare('close', $key_sma_close);
			$key_consecutive_compare = $this->consecutive($key_compare_close_sma, 0);
			$key_ema2 = $this->ema(2, 'close');
			$key_ema4 = $this->ema(4, 'close');
			$key_compare_ema2_ema4 = $this->compare($key_ema2, $key_ema4);

			//list($key_adx, $key_dx, $key_mdi, $key_pdi) = $this->adx(14);

			reset($this->historial);
			foreach ($this->historial as &$h){
				$trend = 'neutral';
				$trend_votes = 0;
				$choppy_votes = 0;

				$total_rate = 20; // bcadd($h[$smaller_key], $h[$greater_key], 1);
				$h[$key_up] = bcmul(100, bcdiv($h[$greater_key], $total_rate, 4), 2);
				$h[$key_down] = bcmul(100, bcdiv($h[$smaller_key], $total_rate, 4), 2);

				// Super obvious case, if one of them is 100, then no need to review more
				if (bccomp($h[$key_up], '100.00', 2) == 0){
					$trend = 'up';
				}
				elseif (bccomp($h[$key_down], '100.00', 2) == 0){
					$trend = 'down';
				}
				else{

					// Check obvious situations
					$obvious_trend = false;
					for ($kk = 100; ($kk >= 80) && ($obvious_trend === false); $kk -= 5){
						$ii = bcdiv((100 - $kk), 5, 0); $ii *= 2;
						// syslog(LOG_INFO|LOG_LOCAL1, 'ii = '.$ii);
						if (	$h[$key_up] >= $kk &&
							$h[$key_compare_close_sma] == 1 &&
							$h[$key_consecutive_compare] >= $ii &&
							$h[$key_compare_ema2_ema4] == 1){
							// syslog(LOG_INFO|LOG_LOCAL1, 'Obvious '.$this->pair.'@'.$h['date'].' UP detected');
							$obvious_trend = 'up';
							break;
						}
						elseif ($h[$key_down] >= $kk &&
							$h[$key_compare_close_sma] == -1 &&
							$h[$key_consecutive_compare] >= $ii &&
							$h[$key_compare_ema2_ema4] == -1){
							$obvious_trend = 'down';
							//syslog(LOG_INFO|LOG_LOCAL1, 'Obvious '.$this->pair.'@'.$h['date'].' DOWN detected');
							break;
						}
					}

					if($obvious_trend === false){

						if (bccomp(bcconv($h[$key_choppy]), 0, 0) == 0){
							$trend_votes++;
						}
						elseif (bccomp(bcconv($h[$key_choppy]), 1, 0) == 0){
							$choppy_votes++;
						}

						if (bccomp($h[$key_chop14], 38.20, 2) <= 0){
							$trend_votes += 2;
						}
						elseif (bccomp($h[$key_chop14], 61.80, 2) >= 0){
							$choppy_votes += 2;
						}

	//					if (bccomp($h[$key_adx], 30, 0) > 0){
	//						$trend_votes++;
	//					}

	//					if (bccomp($h[$key_adx], 30, 0) <= 0){
	//						$choppy_votes++;
	//						}

						if ($h[$key_up] >= 61.8){
							$trend_votes += 2;
						}
						elseif ($h[$key_down] >= 61.8){
							$trend_votes += 2;
						}
						else{
							$choppy_votes += 2;
						}

						if ($trend_votes >= 4){
							if  ($h[$key_down] >= 61.8)
								$trend = 'down';
	//						elseif (bccomp($h[$key_mdi], $h[$key_pdi], EXCHANGE_ROUND_DECIMALS * 2) > 0)
	//							$trend = 'down';
							elseif ($h[$key_up] >= 61.8)
								$trend = 'up';
	//						elseif (bccomp($h[$key_mdi], $h[$key_pdi], EXCHANGE_ROUND_DECIMALS * 2) < 0)
	//							$trend = 'up';
						}
						elseif ($choppy_votes >= 4){
							$trend = 'choppy';
						}
					}
					else{
						$trend = $obvious_trend;
					}
				}

				$h[$key] = $trend;
			}
		}

		$keys = array($key, $key_up, $key_down);
		if ((DEBUG & DEBUG_RATE) && (DEBUG & DEBUG_TRACE_FUNCTIONS_OUTPUT))
			syslog(LOG_INFO|LOG_LOCAL1, "public function rate::detect_trending() = " .  print_r($result,true));
		return $keys;
	}

	public function localtime($timezone='America/Toronto'){
		$date = new DateTime();
		$date->setTimezone(new DateTimeZone($timezone));

		foreach ($this->historial as &$h){
			$date->setTimestamp($h['date']);
			$h['datetime'] = $date->format("Y-m-d H:i:s");
		}
	}

	public function slope($index = 'close', $offset = 1){
        	if ((DEBUG & DEBUG_RATE) && (DEBUG & DEBUG_TRACE_FUNCTIONS))
			syslog(LOG_INFO|LOG_LOCAL1, "public function rate::slope($index, $offset)");

		$key_slope= 'slope('.$index.','.$offset.')';
		$key_sign= 'slope_sign('.$index.','.$offset.')';
	        $t = end($this->historial);

	        if (!array_key_exists($key_slope, $t) || !array_key_exists($key_sign, $t)){
			reset($this->historial); $i = 0;
			$x0 = 0; $x1 = 0 - $offset;
			$exchange_fee = max($this->exchange->make_comission(), $this->exchange->take_comission());
			foreach ($this->historial as &$h){
				$h[$key_sign] = 0;
				$y0 = $h[$index];
				if ($i >= $offset){
					$y1 = $this->historial[$i - $offset][$index];
				}
				else{
					$y1 = $h[$index];
				}
				$h[$key_slope] = bcdiv(bcsub($y0, $y1, EXCHANGE_ROUND_DECIMALS), bcsub($x0, $x1, EXCHANGE_ROUND_DECIMALS),EXCHANGE_ROUND_DECIMALS);
				if (bccomp($h[$key_slope], $exchange_fee) == 1){	// TODO: maths are wrong for key_sign
					$h[$key_sign] = 1;
				}
				elseif(bccomp($h[$key_slope], -1 * $exchange_fee) == -1){
					$h[$key_sign] = -1;
				}
				$i++;
			}
		}

		if ((DEBUG & DEBUG_RATE) && (DEBUG & DEBUG_TRACE_FUNCTIONS_OUTPUT))
			syslog(LOG_INFO|LOG_LOCAL1, "public function rate::slope($index, $offset) = " .  print_r($this->historial,true));

		$keys = array($key_slope, $key_sign);
		return $keys;
	}


	public function ichimoku ($tenkansen = 9, $kijunsen = 26, $chikou = 26, $senkou_b = 52){
        	if ((DEBUG & DEBUG_RATE) && (DEBUG & DEBUG_TRACE_FUNCTIONS))
			syslog(LOG_INFO|LOG_LOCAL1, "public function rate::ichimoku($tenkansen, $kijunsen, $chikou, $senkou_b)");

	        $key_tenkansen = 'tenkansen('.$tenkansen.')';
	        $key_kijunsen = 'kijunsen('.$kijunsen.')';
	        $key_chikou = 'chikou('.$chikou.')';
	        $key_senkou_a = 'senkou_a()';
	        $key_senkou_b = 'senkou_b('.$senkou_b.')';
	        $t = end($this->historial);

	        if (!array_key_exists($key_tenkansen, $t) || !array_key_exists($key_kijunsen, $t) || !array_key_exists($key_chikou, $t) || !array_key_exists($key_senkou_a, $t) || !array_key_exists($key_senkou_b, $t)){
			list($key_min_max_high_min, $key_min_max_high_max, $key_min_max_high_steps_min, $key_min_max_high_steps_max, $key_abs_min_max_high_min, $key_abs_min_max_high_max, $key_abs_min_max_high_steps_min, $key_abs_min_max_high_steps_max) = $this->min_max($tenkansen, 'high', EXCHANGE_ROUND_DECIMALS);
			list($key_min_max_low_min, $key_min_max_low_max, $key_min_max_low_steps_min, $key_min_max_low_steps_max, $key_abs_min_max_low_min, $key_abs_min_max_low_max, $key_abs_min_max_low_steps_min, $key_abs_min_max_low_steps_max) = $this->min_max($tenkansen, 'low', EXCHANGE_ROUND_DECIMALS);

			list($key_min_max_high_min2, $key_min_max_high_max2, $key_min_max_high_steps_min2, $key_min_max_high_steps_max2, $key_abs_min_max_high_min2, $key_abs_min_max_high_max2, $key_abs_min_max_high_steps_min2, $key_abs_min_max_high_steps_max2) = $this->min_max($kijunsen, 'high', EXCHANGE_ROUND_DECIMALS);
			list($key_min_max_low_min2, $key_min_max_low_max2, $key_min_max_low_steps_min2, $key_min_max_low_steps_max2, $key_abs_min_max_low_min2, $key_abs_min_max_low_max2, $key_abs_min_max_low_steps_min2, $key_abs_min_max_low_steps_max2) = $this->min_max($kijunsen, 'low', EXCHANGE_ROUND_DECIMALS);

			list($key_min_max_high_min3, $key_min_max_high_max3, $key_min_max_high_steps_min3, $key_min_max_high_steps_max3, $key_abs_min_max_high_min3, $key_abs_min_max_high_max3, $key_abs_min_max_high_steps_min3, $key_abs_min_max_high_steps_max3) = $this->min_max($senkou_b, 'high', EXCHANGE_ROUND_DECIMALS);
			list($key_min_max_low_min3, $key_min_max_low_max3, $key_min_max_low_steps_min3, $key_min_max_low_steps_max3, $key_abs_min_max_low_min3, $key_abs_min_max_low_max3, $key_abs_min_max_low_steps_min3, $key_abs_min_max_low_steps_max3) = $this->min_max($senkou_b, 'low', EXCHANGE_ROUND_DECIMALS);

			$key_delayed = $this->delayed(26, 'close');
			$this->rename_key($key_delayed, $key_chikou);
			/*
				chikou implementation is different, instead of looking in the future, it looks in the bast.
				this helps the last index to look for the value that was in the past.
			*/

			reset($this->historial); $i = 0;
			foreach ($this->historial as &$h){
				$k = $i - 26;
				$h[$key_tenkansen] = bcdiv(bcadd($h[$key_min_max_high_max], $h[$key_min_max_low_min]), 2, 8);
				$h[$key_kijunsen] = bcdiv(bcadd($h[$key_min_max_high_max2], $h[$key_min_max_low_min2]), 2, 8);
				if ($i >= 26){
					$h[$key_senkou_a] = bcdiv(bcadd($this->historial[$k][$key_tenkansen], $this->historial[$k][$key_kijunsen]), 2, 8);
					$h[$key_senkou_b] = bcdiv(bcadd($this->historial[$k][$key_min_max_high_max3], $this->historial[$k][$key_min_max_low_min3]), 2, 8);
				}
				else{
					$h[$key_senkou_a] = bcdiv(bcadd($h[$key_tenkansen], $h[$key_kijunsen]), 2, 8);
					$h[$key_senkou_b] = bcdiv(bcadd($h[$key_min_max_high_max3], $h[$key_min_max_low_min3]), 2, 8);
				}
				$i++;
			}
        	}

		if ((DEBUG & DEBUG_RATE) && (DEBUG & DEBUG_TRACE_FUNCTIONS_OUTPUT))
			syslog(LOG_INFO|LOG_LOCAL1, "public function rate::ichimoku($tenkansen, $kijunsen, $chikou, $senkou_b) = " .  print_r($this->historial,true));

		$keys = array($key_tenkansen, $key_kijunsen, $key_chikou, $key_senkou_a, $key_senkou_b);
		return $keys;
	}

	/**
	@param float
	@retval boolean
	*/
	public function buy($rate, $amount){
		if ((DEBUG & DEBUG_RATE) && (DEBUG & DEBUG_TRACE_FUNCTIONS))
			syslog(LOG_INFO|LOG_LOCAL1, "public function rate::buy($rate, $amount)");
		$result = $this->exchange->buy($this->pair, $rate, $amount);
		if ((DEBUG & DEBUG_RATE) && (DEBUG & DEBUG_TRACE_FUNCTIONS_OUTPUT))
			syslog(LOG_INFO|LOG_LOCAL1, "public function rate::buy($rate, $amount) = " .  print_r($result,true));
		return $result;
	}

	/**
	@param float
	@retval boolean
	*/
	public function sell($rate, $amount){
		if ((DEBUG & DEBUG_RATE) && (DEBUG & DEBUG_TRACE_FUNCTIONS))
			syslog(LOG_INFO|LOG_LOCAL1, "public function rate::sell($rate, $amount)");
		$result = $this->exchange->sell($this->pair, $rate, $amount);
		if ((DEBUG & DEBUG_RATE) && (DEBUG & DEBUG_TRACE_FUNCTIONS_OUTPUT))
			syslog(LOG_INFO|LOG_LOCAL1, "public function rate::sell($rate, $amount) = " .  print_r($result,true));
		return $result;
	}

	/**
	@retval array
	*/
	public function &get_orders(){
		if ((DEBUG & DEBUG_RATE) && (DEBUG & DEBUG_TRACE_FUNCTIONS))
			syslog(LOG_INFO|LOG_LOCAL1, "public function &rate::get_orders()");
		$result = $this->exchange->get_orders($this->pair);
		//if (is_array($this->book) && count($this->book)){
			$this->book = $result;
			$this->highest_bid = $this->book['bids'][0][0];
			$this->lowest_ask = $this->book['asks'][0][0];
			$this->spread = bcsub($this->lowest_ask, $this->highest_bid, EXCHANGE_ROUND_DECIMALS * 2);
			$this->percent_wise = bcdiv($this->spread, $this->lowest_ask, EXCHANGE_ROUND_DECIMALS * 2);
		//}
		if ((DEBUG & DEBUG_RATE) && (DEBUG & DEBUG_TRACE_FUNCTIONS_OUTPUT))
			syslog(LOG_INFO|LOG_LOCAL1, "public function &rate::get_orders() = " . print_r($result,true));
		return $result;
	}

	/**
	@retval array
	*/
	public function &get_my_orders(){
		if ((DEBUG & DEBUG_RATE) && (DEBUG & DEBUG_TRACE_FUNCTIONS))
			syslog(LOG_INFO|LOG_LOCAL1, "public function &rate::get_my_orders()");
		$result = $this->exchange->get_my_orders($this->pair);
		if ((DEBUG & DEBUG_RATE) && (DEBUG & DEBUG_TRACE_FUNCTIONS_OUTPUT))
			syslog(LOG_INFO|LOG_LOCAL1, "public function &rate::get_my_orders() = " . print_r($result,true));
		return $result;
	}

	/**
	@param int
	Order number
	@retval array
	*/
	public function cancel_order($order_number) {
		if ((DEBUG & DEBUG_RATE) && (DEBUG & DEBUG_TRACE_FUNCTIONS))
			syslog(LOG_INFO|LOG_LOCAL1, "public function rate::cancel_order($order_number)");
		$result = $this->exchange->cancel_order($this->pair, $order_number);
		if ((DEBUG & DEBUG_RATE) && (DEBUG & DEBUG_TRACE_FUNCTIONS_OUTPUT))
			syslog(LOG_INFO|LOG_LOCAL1, "public function rate::cancel_order($order_number) = " . print_r($result,true));
		return $result;
	}

	/**
	@param string
	The pair
	@retval boolean
	true if it exists
	*/

	public function exists($pair){
		if ((DEBUG & DEBUG_RATE) && (DEBUG & DEBUG_TRACE_FUNCTIONS))
			syslog(LOG_INFO|LOG_LOCAL1, "public static function rate::exists($pair)");

		$key = 'exchange['.$this->name.']:ticker';
		if (!is_object($this->exchange->memcached())){
			$_pairs = $this->exchange->api->get_ticker();
		}
		else{
			$_pairs = $this->exchange->memcached()->get($key);
			if ($this->exchange->memcached()->getResultCode() != Memcached::RES_SUCCESS){
				if (DEBUG)
					syslog(LOG_INFO|LOG_LOCAL1, "Ticker NOT found in Memcached: $key");
				$_pairs = $this->exchange->api->get_ticker();
				if (is_array($_pairs)){
					$this->exchange->memcached()->set($key, serialize($_pairs), MEMCACHED_TICKER_TTL);
					if ((DEBUG) && ($this->memcached->getResultCode() == Memcached::RES_NOTSTORED)){
						syslog(LOG_INFO|LOG_LOCAL1, "Ticker SET in Memcached: $key");
					}
				}
				else{
					throw new Exception('Exchangge class '.$this->name.' does not return a ticker.');
				}
			}
			else{
				if (DEBUG){
					syslog(LOG_INFO|LOG_LOCAL1, 'Ticker found in Memcached '.$this->exchange->memcached()->getResultCode());
				}
				$_pairs = unserialize($_pairs);
			}
		}

		// we have all the pairs, now we are looking for them
		$answer = array_key_exists($pair, $_pairs);

		if ((DEBUG & DEBUG_RATE) && (DEBUG & DEBUG_TRACE_FUNCTIONS_OUTPUT))
			syslog(LOG_INFO|LOG_LOCAL1, "public static function rate::exists($pair)");

		return $answer;
	}

	public function take_comission(){
		return $this->exchange->take_comission();
	}

	public function make_comission(){
		return $this->exchange->make_comission();
	}
}
